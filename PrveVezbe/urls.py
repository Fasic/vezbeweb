from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),

    #Izgled: aplikacija/zadatak/5   ID:5
    path('zadaci/', views.zadaci, name='zadaci'),
    path('zadatak/<int:id>', views.get_zadatak, name='get_zadatak'),

    path('zadatak/delete/<int:id>', views.delete_zadatak, name='delete_zadatak'),
    path('zadatak/add', views.add_zadatak, name='add_zadatak'),
    path('zadatak/edit/<int:id>', views.edit_zadatak, name='edit_zadatak'),

    path('api/zadatak/add', views.api_add_zadatak, name='api_add_zadatak'),
    path('api/zadatak/edit/<int:id>', views.api_edit_zadatak, name='api_edit_zadatak'),
]