from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, reverse
from PrveVezbe.models import Zadatak


def index(request):
    return HttpResponse("Probna stranica 1!")


def get_zadatak(request, id):
    zadatak = get_object_or_404(Zadatak, pk=id)
    return render(request, 'zadatak.html', {"zadatak": zadatak})


def delete_zadatak(request, id):
    zadatak = get_object_or_404(Zadatak, pk=id)
    zadatak.delete()
    return HttpResponseRedirect(reverse('zadaci'))


def zadaci(request):
    tasks = Zadatak.objects.all()
    return render(request, 'zadaci.html', {"tasks": tasks})


def add_zadatak(request):
    return render(request, 'add_zadatak.html')


def edit_zadatak(request, id):
    zadatak = get_object_or_404(Zadatak, pk=id)
    return render(request, 'edit_zadatak.html', {"zadatak": zadatak})


def api_add_zadatak(request):
    zadatak = Zadatak(tekst=request.POST['tekst'], opis=request.POST['opis'])
    zadatak.save()
    return HttpResponseRedirect(reverse('zadaci'))


def api_edit_zadatak(request, id):
    zadatak = Zadatak.objects.get(pk=id)

    zadatak.tekst = request.POST['tekst']
    zadatak.opis = request.POST['opis']

    zadatak.save()
    return HttpResponseRedirect(reverse('zadaci'))
