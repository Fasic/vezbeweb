from django.db import models

# Create your models here.
class Zadatak(models.Model):
    tekst = models.CharField(max_length=200)
    opis = models.CharField(max_length=2000)

    def __str__(self):
        return "Naslov: " + self.tekst + "\n" + "Opis: " + self.opis

    #Dodavanje
    #Brisanje
    #GetAll - svi
    #GetByID - jedan

    #EDIT
    #ispravljanje linkova